package cmsc433.p1;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;

public class Server
{
    /**
     * Singleton: the following code makes the server a Singleton. You should
     * not edit this code.
     * 
     * For test purposes, we made the constructor protected. 
     */

    /* Singleton: Begin */
    protected Server()
    {
    }

    private static Server instance = new Server();

    public static Server getInstance()
    {
        return instance;
    }

    /* Singleton: End */
    
    /**
     * Server statistics:
     */
    private int revenue = 0;
    private int soldItemsCount = 0;

    /**
     * Server restrictions:
     */
    public static final int maxBidCount = 10; // The maximum number of bids at any given time for a buyer.
    public static final int maxSellerItems = 20; // The maximum number of items that a seller can submit at any given time.
    public static final int serverCapacity = 80; // The maximum number of active items at a given time.
    
    private Hashtable<String, Integer> sellerProducts = new Hashtable<String, Integer>(); // Stores the number of active bids for each seller.

    private int lastListingID = -1; // The last used listing ID assigned to an item. 
    
    // listingID, bid
    private Hashtable<Integer, Integer> highestBids = new Hashtable<Integer, Integer>(); // Stores the highest bid for each item (includes expired bids).
    
    // listingID, buyer
    private Hashtable<Integer, String> highestBidders = new Hashtable<Integer, String>(); // Stores the buyer which submitted the highest bid for each item.
    
    // buyer, bid count
    private Hashtable<String, Integer> buyerBidCounts = new Hashtable<String, Integer>(); // Stores the number of active bids for each buyer.
    
    // listingID, item
    private Hashtable<Integer, Item> items = new Hashtable<Integer, Item>(); // Stores a mapping between item IDs and items. (Includes expired items.)
    
    private List<Item> openBids = new ArrayList<Item>();
    
    private Object instanceLock = new Object(); // Object used for instance synchronization. (As a good practice we don't use synchronized (this).)
    
    public int soldItemsCount()
    {
        return this.soldItemsCount;
    }

    public int revenue()
    {
        return this.revenue;
    }

    public int submitItem(String clientName, String itemName, int lowestBiddingPrice, int biddingDurationMs)
    {
        // TODO: Modify this code so that the project requirements are met, 
        //       the unit tests pass, and the code is thread-safe.        
        
        if (this.openBids.size() == serverCapacity) 
        { 
            return -1; 
        }
        
        Integer count = this.sellerProducts.get(clientName);
        if (count == null)
        {
            count = 0;
            this.sellerProducts.put(clientName, count);
        }

        if (count == maxSellerItems) { return -1; }

        Item item = new Item(clientName, itemName, ++this.lastListingID, lowestBiddingPrice, biddingDurationMs);
        this.items.put(this.lastListingID, item);
        this.openBids.add(item);
        this.highestBids.put(this.lastListingID, lowestBiddingPrice);
        
        return this.lastListingID;
    }

    public List<Item> getItems()
    {
        // TODO: Modify this code so that the project requirements are met, 
        //       the unit tests pass, and the code is thread-safe.
        
        return this.openBids;
    }

    public boolean submitBid(String clientName, int listingID, int biddingAmount)
    {
        // TODO: Modify this code so that the project requirements are met, 
        //       the unit tests pass, and the code is thread-safe.
        
        Item item = this.items.get(listingID);
        if (!item.biddingOpen()) 
        { 
            // Bidding closed for this item
            return false; 
        }
        
        Integer highestBid = this.highestBids.get(listingID);
        if (highestBid != null && highestBid >= biddingAmount)
        {
            // Bid amount is too small
            return false;
        }
        
        Integer buyerBidCount = this.buyerBidCounts.get(clientName); 
        if (buyerBidCount != null && buyerBidCount >= maxBidCount)
        {
            // This buyer reached his limit of bids for different items
            return false;
        }
        
        if (buyerBidCount == null)
        {
            buyerBidCount = 0;
        }
        
        this.buyerBidCounts.put(clientName, buyerBidCount+1);
        
        this.highestBids.put(listingID, biddingAmount);
        this.highestBidders.put(listingID, clientName);
        
        return true;
    }

    public int checkBidStatus(String clientName, int listingID)
    {
        // TODO: Modify this code so that the project requirements are met, 
        //       the unit tests pass, and the code is thread-safe.
        
        Item item = this.items.get(listingID);
        
        if (!item.biddingOpen())
        {
            String highestBidder = this.highestBidders.get(item.listingID());
            
            if (this.openBids.contains(item))
            {
                int price = this.highestBids.get(item.listingID());
                // TODO: Update statistics here.
                
                this.openBids.remove(item);
            }    
            
            // Update the number of open bids for this seller
            int sellerProductsCount = this.sellerProducts.get(item.seller());
            this.sellerProducts.put(item.seller(), sellerProductsCount-1);
            
            // Bidding for this item is over
            if (highestBidder != null && highestBidder.equals(clientName))
            {
                // SUCCESS
                return 1;
            } 
            else
            {
                // FAILED
                return 3;
            }
        }
        else
        {
            // OPEN
            return 2;
        }
    }

    public int itemPrice(int listingID)
    {
        // TODO: Modify this code so that the project requirements are met, 
        //       the unit tests pass, and the code is thread-safe.
        Integer price = this.highestBids.get(listingID);
        
        if (price == null)
        {
            Item item = this.items.get(listingID);
            price = item.lowestBiddingPrice();
        }
        
        return price;
    }
}
