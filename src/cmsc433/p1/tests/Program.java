package cmsc433.p1.tests;

import cmsc433.p1.*;

/**
 * Class provided for ease of test. This will not be used in the project 
 * evaluation, so feel free to modify it as you like.
 */ 
public class Program
{
    public static void main(String[] args)
    {        
        // Use the following statements to redirect the log to a stream different from stdout:
        // ByteArrayOutputStream os = new ByteArrayOutputStream();
        // PrintStream ps = new PrintStream(os);
        // Logger.getInstance().setOutput(ps);
        
        int nrSellers = 50;
        int nrBuyers = 20;
        
        Thread[] sellerThreads = new Thread[nrSellers];
        Thread[] buyerThreads = new Thread[nrBuyers];
        Seller[] sellers = new Seller[nrSellers];
        Buyer[] buyers = new Buyer[nrBuyers];
        
        for (int i=0; i<nrSellers; ++i)
        {
            sellers[i] = new Seller(ServerPrinter.getInstance(), "Seller"+i, 100, 50, i);
            sellerThreads[i] = new Thread(sellers[i]);
            sellerThreads[i].start();
        }
        
        for (int i=0; i<nrBuyers; ++i)
        {
            buyers[i] = new Buyer(ServerPrinter.getInstance(), "Buyer"+i, 1000, 20, 150, i);
            buyerThreads[i] = new Thread(buyers[i]);
            buyerThreads[i].start();
        }
        
        for (int i=0; i<nrSellers; ++i)
        {
            try
            {
                sellerThreads[i].join();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        
        int moneySpent = 0;
        for (int i=0; i<nrBuyers; ++i)
        {
            try
            {
                buyerThreads[i].join();
                moneySpent += buyers[i].cashSpent();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        
        // Use the following statements to redirect the log to a stream different from stdout:
        // String output = os.toString();
        // FileWriter writer;
        // try
        // {
        //     // Output the log of the test in a separate file, for ease of debug.
        //     writer = new FileWriter("out.txt");
        //     writer.write(output);
        //     writer.close();
        // }
        // catch (IOException e)
        // {
        //     e.printStackTrace();
        // }

        System.out.println("The following two numbers should be the same:");
        System.out.println("The total sum of money spent, reported by te buyers is " + moneySpent);
        System.out.println("The total revenue reported by the server is " + ServerPrinter.getInstance().revenue());
        System.out.println();
        
        System.out.println("The following numbers may differ, but may not exceed 100:");
        for (int i=0; i<buyers.length; ++i)
        {
            System.out.println("The most items available to bid for, recorded by " + buyers[i].name() + " is " + buyers[i].mostItemsAvailable());
        }
    }
}
