package cmsc433.p1.tests;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cmsc433.p1.*;

public class ServerTestSingleThread
{

    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
        // Before each test, reset the Server, by re-initializing its instance.
        Constructor<Server> serverConstructor = Server.class.getDeclaredConstructor((Class<Server>[])null);
        serverConstructor.setAccessible(true);
        
        Field serverInstance = Server.class.getDeclaredField("instance");
        serverInstance.setAccessible(true);
        
        serverInstance.set(null, serverConstructor.newInstance((Object[])null));
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testMaxCounts()
    {
        Server server = Server.getInstance();

        assertEquals(0, server.getItems().size());
        
        for (int j=0; j<20; ++j)
        {
            for (int i=0; i<100; ++i)
            {
                server.submitItem("Client"+j, "Item" + i, 1, 5);
            }
            
            List<Item> items = server.getItems();
            assertEquals(Math.min(Server.maxSellerItems*(j+1), Server.serverCapacity), items.size());
        }
    }
    
    @Test
    public void testUniqueIDs()
    {
        Server server = Server.getInstance();
                
        assertEquals(0, server.getItems().size());
        
        HashSet<Integer> ids = new HashSet<Integer>();
        
        for (int j=0; j<20; ++j)
        {
            for (int i=0; i<Server.maxSellerItems*10; ++i)
            {
                int id = server.submitItem("Client"+j, "Item" + i, 1, 5);
                
                if (i>=Server.maxSellerItems || j>=Server.serverCapacity / Server.maxSellerItems)
                {
                    assertEquals(-1, id);
                }
                else
                {
                    assertFalse(ids.contains(id));
                    ids.add(id);
                }
            }
        }
    }
    
    @Test
    public void testBiddingOpen()
    {
        Server server = Server.getInstance();
        
        assertEquals(0, server.getItems().size());
        
        List<Integer> ids = new ArrayList<Integer>();
        
        int sellers = Server.serverCapacity / Server.maxSellerItems;
        for (int j=0; j<sellers; ++j)
        {
            for (int i=0; i<Server.maxSellerItems; ++i)
            {
                int id = server.submitItem("Client"+j, "Item" + i, 1, 5000);
                
                ids.add(id);
            }
        }
        
        for (Integer id : ids)
        {
            assertEquals(2, server.checkBidStatus("Buyer", id));
        }
    }
    
    @Test
    public void testBiddingFailed()
    {
        Server server = Server.getInstance();
        
        assertEquals(0, server.getItems().size());
        
        List<Integer> ids = new ArrayList<Integer>();
        
        int sellers = Server.serverCapacity / Server.maxSellerItems;
        for (int j=0; j<sellers; ++j)
        {
            for (int i=0; i<Server.maxSellerItems; ++i)
            {
                int id = server.submitItem("Client"+j, "Item" + i, 1, 1);
                
                ids.add(id);
            }
        }
        
        try
        {
            Thread.sleep(5);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        for (Integer id : ids)
        {
            assertEquals(3, server.checkBidStatus("Buyer", id));
        }
    }
    
    @Test
    public void testBiddingSuccess()
    {
        Server server = Server.getInstance();
        
        assertEquals(0, server.getItems().size());
        
        List<Integer> ids = new ArrayList<Integer>();
        
        int sellers = Server.serverCapacity / Server.maxSellerItems;
        for (int j=0; j<sellers; ++j)
        {
            for (int i=0; i<Server.maxSellerItems; ++i)
            {
                int id = server.submitItem("Client"+j, "Item" + i, 1, 10);
                
                ids.add(id);
            }
        }
        
        for (Integer id : ids)
        {
            server.submitBid("Buyer" + id, id, 2);
        }
        
        try
        {
            Thread.sleep(50);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        for (Integer id : ids)
        {
            assertEquals(1, server.checkBidStatus("Buyer" + id, id));
        }
    }
    
    @Test
    public void testBiddingLimits()
    {
        Server server = Server.getInstance();
        
        assertEquals(0, server.getItems().size());
        
        List<Integer> ids = new ArrayList<Integer>();
        
        int sellers = Server.serverCapacity/Server.maxSellerItems;
        for (int j=0; j<sellers; ++j)
        {
            for (int i=0; i<Server.maxSellerItems; ++i)
            {
                int id = server.submitItem("Client"+j, "Item" + i, 1, 10);
                
                ids.add(id);
            }
        }
        
        for (int j=0; j<10; ++j)
        {
            int id = server.submitItem("Client"+j, "ItemInfinity", 1, 1);
            
            assertEquals(-1, id);
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            int id = ids.get(i);
            server.submitBid("Buyer1", id, 3);
            server.submitBid("Buyer2", id, 1);
            if (i>=2)
            {
                server.submitBid("Buyer3", id, 2);
            }
        }
        
        try
        {
            Thread.sleep(50);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            if (i<Server.maxBidCount)
            {
                assertEquals(1, server.checkBidStatus("Buyer1", ids.get(i)));
            }
            else
            {
                assertEquals(3, server.checkBidStatus("Buyer1", ids.get(i)));
            }
            
            if (i>=Server.maxBidCount && i<Server.maxBidCount*2)
            {
                assertEquals(1, server.checkBidStatus("Buyer3", ids.get(i)));
            }
            else
            {
                assertEquals(3, server.checkBidStatus("Buyer3", ids.get(i)));
            }
            
            assertEquals(3, server.checkBidStatus("Buyer2", ids.get(i)));
        }
        
        ids.clear();
        for (int j=0; j<sellers; ++j)
        {
            for (int i=0; i<Server.maxSellerItems; ++i)
            {
                int id = server.submitItem("Client"+j, "Item" + i, 1, 10);
                
                assertTrue("i=" + i + "; j=" + j, id >= 0);
                
                ids.add(id);
            }
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            boolean result;
            int id = ids.get(i);
            result = server.submitBid("Buyer1", id, 4);
            if (i<Server.maxBidCount) { assertEquals(true, result); }
            else { assertEquals(false, result); }
            result = server.submitBid("Buyer2", id, 2);
            if (i>=Server.maxBidCount && i<Server.maxBidCount*3) { assertEquals(true, result); }
            else { assertEquals(false, result); }
            if (i>=2)
            {
                result = server.submitBid("Buyer3", id, 3);
                if (i>=Server.maxBidCount && i<Server.maxBidCount*2) { assertEquals(true, result); }
                else { assertEquals(false, result); }
            }
        }
    }
    
    @Test
    public void testBiddingPrice()
    {
        Server server = Server.getInstance();
        
        assertEquals(0, server.getItems().size());
        
        List<Integer> ids = new ArrayList<Integer>();
        
        int sellers = Server.serverCapacity/Server.maxSellerItems;
        for (int j=0; j<sellers; ++j)
        {
            for (int i=0; i<Server.maxSellerItems; ++i)
            {
                int id = server.submitItem("Client"+j, "Item" + i, 1, 10);
                
                ids.add(id);
            }
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            int id = ids.get(i);
            boolean expected;
            if (i<Server.maxBidCount) expected = true;
            else expected = false;
                
            assertEquals(expected, server.submitBid("Buyer1", id, 3));
            assertEquals(false, server.submitBid("Buyer1", id, 2));
            assertEquals(false, server.submitBid("Buyer2", id, 1));
            
            if (i>=2)
            {
                if (i<Server.maxBidCount*3 && i>=Server.maxBidCount) expected = true;
                else expected = false;
                assertEquals(expected, server.submitBid("Buyer3", id, 2));
            }
            if (i>=Server.maxBidCount+2)
            {
                if (i<Server.maxBidCount*2 + 2) expected = true;
                else expected = false;
                assertEquals(expected, server.submitBid("Buyer4", id, 4));
                
                expected = false;
                assertEquals(expected, server.submitBid("Buyer4", id, 5));
            }
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            int id = ids.get(i);
            if (i<Server.maxBidCount)
            {
                assertEquals(3, server.itemPrice(id));
            }
            else if (i<Server.maxBidCount+2)
            {
                assertEquals(2, server.itemPrice(id));
            } 
            else if (i<Server.maxBidCount*2 + 2)
            {
                assertEquals(4, server.itemPrice(id));
            }
            else if (i<Server.maxBidCount*3)
            {
                assertEquals(2, server.itemPrice(id));
            }
            else
            {
                assertEquals(1, server.itemPrice(id));
            }
        }
    }
    
    @Test
    public void testStatistics()
    {
        Server server = Server.getInstance();
        
        assertEquals(0, server.getItems().size());
        
        List<Integer> ids = new ArrayList<Integer>();
        
        int sellers = Server.serverCapacity/Server.maxSellerItems;
        for (int j=0; j<sellers; ++j)
        {
            for (int i=0; i<Server.maxSellerItems; ++i)
            {
                int k = (i<Server.maxSellerItems/2) ? 1 : 2;
                int id = server.submitItem("Client"+j, "Item" + i, 1, (1+i%2)*100);
                
                ids.add(id);
            }
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            int id = ids.get(i);
            server.submitBid("Buyer1", id, 3);
            server.submitBid("Buyer2", id, 1);
            if (i>=2)
            {
                server.submitBid("Buyer3", id, 2);
            }
        }
        
        try
        {
            Thread.sleep(150);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            int id = ids.get(i);
            server.checkBidStatus("Buyer1", id); // Force the server to update the statistics
        }
        
        assertEquals(Server.maxBidCount, server.soldItemsCount());
        assertEquals((int)(Math.floor((double)Server.maxBidCount/2.0)*2 + Math.ceil((double)Server.maxBidCount/2.0)*3), server.revenue());
        
        try
        {
            Thread.sleep(200);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        for (int i=0; i<ids.size(); ++i)
        {
            int id = ids.get(i);
            server.checkBidStatus("Buyer1", id); // Force the server to update the statistics
        }
        
        assertEquals(2*Server.maxBidCount, server.soldItemsCount());
        assertEquals((3+2)*Server.maxBidCount, server.revenue());
    }
}
