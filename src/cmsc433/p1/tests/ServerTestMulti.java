package cmsc433.p1.tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cmsc433.p1.*;

public class ServerTestMulti
{

    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
        // Before each test, reset the Server, by re-initializing its instance. 
        Constructor<ServerPrinter> serverConstructor = ServerPrinter.class.getDeclaredConstructor((Class<ServerPrinter>[])null);
        serverConstructor.setAccessible(true);
        
        Field serverInstance = ServerPrinter.class.getDeclaredField("instance");
        serverInstance.setAccessible(true);
        
        serverInstance.set(null, serverConstructor.newInstance((Object[])null));
    }

    @After
    public void tearDown() throws Exception
    {
    }

    private void testMultiThread(int testNumber, int nrSellers, int nrBuyers)
    {   
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);

        Logger.getInstance().setOutput(ps);
        
        Thread[] sellerThreads = new Thread[nrSellers];
        Thread[] buyerThreads = new Thread[nrBuyers];
        Seller[] sellers = new Seller[nrSellers];
        Buyer[] buyers = new Buyer[nrBuyers];
        
        for (int i=0; i<nrSellers; ++i)
        {
            Server serverPrinter = ServerPrinter.getInstance();
            
            sellers[i] = new Seller(
                    /* server = */          serverPrinter, 
                    /* sellerName = */      "Seller"+i, 
                    /* cycles = */          100, 
                    /* maxSleepTimeMs = */  2, 
                    /* randomSeed = */      i);
            sellerThreads[i] = new Thread(sellers[i]);
            sellerThreads[i].start();
        }
        
        for (int i=0; i<nrBuyers; ++i)
        {
            buyers[i] = new Buyer(
                    /* server = */          ServerPrinter.getInstance(), 
                    /* buyerName = */       "Buyer"+i, 
                    /* initialCash = */     1000, 
                    /* cycles = */          20, 
                    /* maxSleepTimeMs = */  2, 
                    /* randomSeed = */      i);
            buyerThreads[i] = new Thread(buyers[i]);
            buyerThreads[i].start();
        }
        
        for (int i=0; i<nrSellers; ++i)
        {
            try
            {
                sellerThreads[i].join();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        
        int moneySpent = 0;
        for (int i=0; i<nrBuyers; ++i)
        {
            try
            {
                buyerThreads[i].join();
                moneySpent += buyers[i].cashSpent();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        
//        String output = os.toString();
//        
//        FileWriter writer;
//        try
//        {
//            // Output the log of the test in a separate file, for ease of debugging.
//            writer = new FileWriter("out" + testNumber + ".txt");
//            writer.write(output);
//            writer.close();
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
        
        assertTrue("For test number " + testNumber + " the server revenue " + ServerPrinter.getInstance().revenue() + " differs from the revenue reported by the buyers " + moneySpent + "!", moneySpent == ServerPrinter.getInstance().revenue());
        for (int i=0; i<buyers.length; ++i)
        {
            assertTrue("For test number " + testNumber + " the server items capacity " + buyers[i].mostItemsAvailable() + " exceeds the limit of " + Server.serverCapacity + "!", buyers[i].mostItemsAvailable() <= Server.serverCapacity);
        }
    }
    
    @Test
    public void testMultiThread0()
    {
    testMultiThread(0, 80, 80);
    }

    @Test
    public void testMultiThread1()
    {
    testMultiThread(1, 80, 160);
    }

    @Test
    public void testMultiThread2()
    {
    testMultiThread(2, 80, 240);
    }

    @Test
    public void testMultiThread3()
    {
    testMultiThread(3, 80, 320);
    }

    @Test
    public void testMultiThread4()
    {
    testMultiThread(4, 80, 400);
    }

    @Test
    public void testMultiThread5()
    {
    testMultiThread(5, 80, 480);
    }

    @Test
    public void testMultiThread6()
    {
    testMultiThread(6, 80, 560);
    }

    @Test
    public void testMultiThread7()
    {
    testMultiThread(7, 80, 640);
    }

    @Test
    public void testMultiThread8()
    {
    testMultiThread(8, 80, 720);
    }

    @Test
    public void testMultiThread9()
    {
    testMultiThread(9, 80, 800);
    }

    @Test
    public void testMultiThread10()
    {
    testMultiThread(10, 160, 80);
    }

    @Test
    public void testMultiThread11()
    {
    testMultiThread(11, 160, 160);
    }

    @Test
    public void testMultiThread12()
    {
    testMultiThread(12, 160, 240);
    }

    @Test
    public void testMultiThread13()
    {
    testMultiThread(13, 160, 320);
    }

    @Test
    public void testMultiThread14()
    {
    testMultiThread(14, 160, 400);
    }

    @Test
    public void testMultiThread15()
    {
    testMultiThread(15, 160, 480);
    }

    @Test
    public void testMultiThread16()
    {
    testMultiThread(16, 160, 560);
    }

    @Test
    public void testMultiThread17()
    {
    testMultiThread(17, 160, 640);
    }

    @Test
    public void testMultiThread18()
    {
    testMultiThread(18, 160, 720);
    }

    @Test
    public void testMultiThread19()
    {
    testMultiThread(19, 160, 800);
    }

    @Test
    public void testMultiThread20()
    {
    testMultiThread(20, 240, 80);
    }

    @Test
    public void testMultiThread21()
    {
    testMultiThread(21, 240, 160);
    }

    @Test
    public void testMultiThread22()
    {
    testMultiThread(22, 240, 240);
    }

    @Test
    public void testMultiThread23()
    {
    testMultiThread(23, 240, 320);
    }

    @Test
    public void testMultiThread24()
    {
    testMultiThread(24, 240, 400);
    }

    @Test
    public void testMultiThread25()
    {
    testMultiThread(25, 240, 480);
    }

    @Test
    public void testMultiThread26()
    {
    testMultiThread(26, 240, 560);
    }

    @Test
    public void testMultiThread27()
    {
    testMultiThread(27, 240, 640);
    }

    @Test
    public void testMultiThread28()
    {
    testMultiThread(28, 240, 720);
    }

    @Test
    public void testMultiThread29()
    {
    testMultiThread(29, 240, 800);
    }

    @Test
    public void testMultiThread30()
    {
    testMultiThread(30, 320, 80);
    }

    @Test
    public void testMultiThread31()
    {
    testMultiThread(31, 320, 160);
    }

    @Test
    public void testMultiThread32()
    {
    testMultiThread(32, 320, 240);
    }

    @Test
    public void testMultiThread33()
    {
    testMultiThread(33, 320, 320);
    }

    @Test
    public void testMultiThread34()
    {
    testMultiThread(34, 320, 400);
    }

    @Test
    public void testMultiThread35()
    {
    testMultiThread(35, 320, 480);
    }

    @Test
    public void testMultiThread36()
    {
    testMultiThread(36, 320, 560);
    }

    @Test
    public void testMultiThread37()
    {
    testMultiThread(37, 320, 640);
    }

    @Test
    public void testMultiThread38()
    {
    testMultiThread(38, 320, 720);
    }

    @Test
    public void testMultiThread39()
    {
    testMultiThread(39, 320, 800);
    }

    @Test
    public void testMultiThread40()
    {
    testMultiThread(40, 400, 80);
    }

    @Test
    public void testMultiThread41()
    {
    testMultiThread(41, 400, 160);
    }

    @Test
    public void testMultiThread42()
    {
    testMultiThread(42, 400, 240);
    }

    @Test
    public void testMultiThread43()
    {
    testMultiThread(43, 400, 320);
    }

    @Test
    public void testMultiThread44()
    {
    testMultiThread(44, 400, 400);
    }

    @Test
    public void testMultiThread45()
    {
    testMultiThread(45, 400, 480);
    }

    @Test
    public void testMultiThread46()
    {
    testMultiThread(46, 400, 560);
    }

    @Test
    public void testMultiThread47()
    {
    testMultiThread(47, 400, 640);
    }

    @Test
    public void testMultiThread48()
    {
    testMultiThread(48, 400, 720);
    }

    @Test
    public void testMultiThread49()
    {
    testMultiThread(49, 400, 800);
    }

    @Test
    public void testMultiThread50()
    {
    testMultiThread(50, 480, 80);
    }

    @Test
    public void testMultiThread51()
    {
    testMultiThread(51, 480, 160);
    }

    @Test
    public void testMultiThread52()
    {
    testMultiThread(52, 480, 240);
    }

    @Test
    public void testMultiThread53()
    {
    testMultiThread(53, 480, 320);
    }

    @Test
    public void testMultiThread54()
    {
    testMultiThread(54, 480, 400);
    }

    @Test
    public void testMultiThread55()
    {
    testMultiThread(55, 480, 480);
    }

    @Test
    public void testMultiThread56()
    {
    testMultiThread(56, 480, 560);
    }

    @Test
    public void testMultiThread57()
    {
    testMultiThread(57, 480, 640);
    }

    @Test
    public void testMultiThread58()
    {
    testMultiThread(58, 480, 720);
    }

    @Test
    public void testMultiThread59()
    {
    testMultiThread(59, 480, 800);
    }

    @Test
    public void testMultiThread60()
    {
    testMultiThread(60, 560, 80);
    }

    @Test
    public void testMultiThread61()
    {
    testMultiThread(61, 560, 160);
    }

    @Test
    public void testMultiThread62()
    {
    testMultiThread(62, 560, 240);
    }

    @Test
    public void testMultiThread63()
    {
    testMultiThread(63, 560, 320);
    }

    @Test
    public void testMultiThread64()
    {
    testMultiThread(64, 560, 400);
    }

    @Test
    public void testMultiThread65()
    {
    testMultiThread(65, 560, 480);
    }

    @Test
    public void testMultiThread66()
    {
    testMultiThread(66, 560, 560);
    }

    @Test
    public void testMultiThread67()
    {
    testMultiThread(67, 560, 640);
    }

    @Test
    public void testMultiThread68()
    {
    testMultiThread(68, 560, 720);
    }

    @Test
    public void testMultiThread69()
    {
    testMultiThread(69, 560, 800);
    }

    @Test
    public void testMultiThread70()
    {
    testMultiThread(70, 640, 80);
    }

    @Test
    public void testMultiThread71()
    {
    testMultiThread(71, 640, 160);
    }

    @Test
    public void testMultiThread72()
    {
    testMultiThread(72, 640, 240);
    }

    @Test
    public void testMultiThread73()
    {
    testMultiThread(73, 640, 320);
    }

    @Test
    public void testMultiThread74()
    {
    testMultiThread(74, 640, 400);
    }

    @Test
    public void testMultiThread75()
    {
    testMultiThread(75, 640, 480);
    }

    @Test
    public void testMultiThread76()
    {
    testMultiThread(76, 640, 560);
    }

    @Test
    public void testMultiThread77()
    {
    testMultiThread(77, 640, 640);
    }

    @Test
    public void testMultiThread78()
    {
    testMultiThread(78, 640, 720);
    }

    @Test
    public void testMultiThread79()
    {
    testMultiThread(79, 640, 800);
    }

    @Test
    public void testMultiThread80()
    {
    testMultiThread(80, 720, 80);
    }

    @Test
    public void testMultiThread81()
    {
    testMultiThread(81, 720, 160);
    }

    @Test
    public void testMultiThread82()
    {
    testMultiThread(82, 720, 240);
    }

    @Test
    public void testMultiThread83()
    {
    testMultiThread(83, 720, 320);
    }

    @Test
    public void testMultiThread84()
    {
    testMultiThread(84, 720, 400);
    }

    @Test
    public void testMultiThread85()
    {
    testMultiThread(85, 720, 480);
    }

    @Test
    public void testMultiThread86()
    {
    testMultiThread(86, 720, 560);
    }

    @Test
    public void testMultiThread87()
    {
    testMultiThread(87, 720, 640);
    }

    @Test
    public void testMultiThread88()
    {
    testMultiThread(88, 720, 720);
    }

    @Test
    public void testMultiThread89()
    {
    testMultiThread(89, 720, 800);
    }

    @Test
    public void testMultiThread90()
    {
    testMultiThread(90, 800, 80);
    }

    @Test
    public void testMultiThread91()
    {
    testMultiThread(91, 800, 160);
    }

    @Test
    public void testMultiThread92()
    {
    testMultiThread(92, 800, 240);
    }

    @Test
    public void testMultiThread93()
    {
    testMultiThread(93, 800, 320);
    }

    @Test
    public void testMultiThread94()
    {
    testMultiThread(94, 800, 400);
    }

    @Test
    public void testMultiThread95()
    {
    testMultiThread(95, 800, 480);
    }

    @Test
    public void testMultiThread96()
    {
    testMultiThread(96, 800, 560);
    }

    @Test
    public void testMultiThread97()
    {
    testMultiThread(97, 800, 640);
    }

    @Test
    public void testMultiThread98()
    {
    testMultiThread(98, 800, 720);
    }

    @Test
    public void testMultiThread99()
    {
    testMultiThread(99, 800, 800);
    }
}
