package cmsc433.p1.tests;

import java.io.FileWriter;
import java.io.IOException;

public class TestFileGenerator
{
    public static void main(String[] args)
    {
        // Generate test files
        String c = 
        "package cmsc433.p1.tests;\n\n" + 
        "import java.lang.reflect.Constructor;\n" +
        "import java.lang.reflect.Field;\n" + 
        "import org.junit.Before;\n" + 
        "import org.junit.BeforeClass;\n" + 
        "import org.junit.Test;\n" + 

        "import cmsc433.p1.*;\n" + 

        "public class ServerTestMultiThread%d\n" + 
        "{\n" + 
        "    @BeforeClass\n" + 
        "    public static void setUpBeforeClass() throws Exception\n" + 
        "    {\n" + 
        "        Server.getInstance().stop();\n" + 
        "    }\n\n" + 
        "    @Before\n" + 
        "    public void setUp() throws Exception\n" + 
        "    {\n" + 
        "        // Before each test, reset the Server, by re-initializing its instance. \n" + 
        "        Constructor<ServerPrinter> serverConstructor = ServerPrinter.class.getDeclaredConstructor((Class<ServerPrinter>[])null);\n" + 
        "        serverConstructor.setAccessible(true);\n" +      
        "        Field serverInstance = ServerPrinter.class.getDeclaredField(\"instance\");\n" + 
        "        serverInstance.setAccessible(true);\n\n" + 
        "        serverInstance.set(null, serverConstructor.newInstance((Object[])null));\n" + 
        "    }\n\n" +
        "    @Test\n" + 
        "    public void testMultiThread%d()\n" + 
        "    {\n" + 
        "        ServerTestHelper.testMultiThread(%d, %d, %d);\n" + 
        "    }\n" + 
        "}\n";
        
        int tn = 0;
        for (int s = 80; s <= 800; s += 80)
        {
            for (int b = 80; b <= 800; b += 80, ++tn)
            {
                 System.out.printf("    @Test\n    public void testMultiThread%d()\n    {\n    testMultiThread(%d, %d, %d);\n    }\n\n",
                 tn, tn, s, b);
//                try
//                {
//                    FileWriter writer = new FileWriter("ServerTestMultiThread" + tn + ".java");
//                    String content = String.format(c, tn, tn, tn, s, b);
//                    writer.write(content);
//                    writer.close();
//                }
//                catch (IOException e)
//                {
//                    e.printStackTrace();
//                }
            }
        }

    }

}
