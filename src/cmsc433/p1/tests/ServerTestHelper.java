package cmsc433.p1.tests;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

import cmsc433.p1.Buyer;
import cmsc433.p1.Seller;
import cmsc433.p1.Server;

public class ServerTestHelper
{
    public static void testMultiThread(int testNumber, int nrSellers, int nrBuyers)
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);

        Logger.getInstance().setOutput(ps);
        
        Thread[] sellerThreads = new Thread[nrSellers];
        Thread[] buyerThreads = new Thread[nrBuyers];
        Seller[] sellers = new Seller[nrSellers];
        Buyer[] buyers = new Buyer[nrBuyers];
        
        for (int i=0; i<nrSellers; ++i)
        {
            Server serverPrinter = ServerPrinter.getInstance();
            
            sellers[i] = new Seller(
                    /* server = */          serverPrinter, 
                    /* sellerName = */      "Seller"+i, 
                    /* cycles = */          100, 
                    /* maxSleepTimeMs = */  50, 
                    /* randomSeed = */      i);
            sellerThreads[i] = new Thread(sellers[i]);
            sellerThreads[i].start();
        }
        
        for (int i=0; i<nrBuyers; ++i)
        {
            buyers[i] = new Buyer(
                    /* server = */          ServerPrinter.getInstance(), 
                    /* buyerName = */       "Buyer"+i, 
                    /* initialCash = */     500, 
                    /* cycles = */          20, 
                    /* maxSleepTimeMs = */  150, 
                    /* randomSeed = */      i);
            buyerThreads[i] = new Thread(buyers[i]);
            buyerThreads[i].start();
        }
        
        for (int i=0; i<nrSellers; ++i)
        {
            try
            {
                sellerThreads[i].join();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        
        int moneySpent = 0;
        for (int i=0; i<nrBuyers; ++i)
        {
            try
            {
                buyerThreads[i].join();
                moneySpent += buyers[i].cashSpent();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        
        String output = os.toString();
        
        FileWriter writer;
        try
        {
            // Output the log of the test in a separate file, for ease of debugging.
            writer = new FileWriter("out" + testNumber + ".txt");
            writer.write(output);
            writer.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        
        assertTrue("For test number " + testNumber + " the server revenue " + ServerPrinter.getInstance().revenue() + " differs from the revenue reported by the buyers " + moneySpent + "!", moneySpent == ServerPrinter.getInstance().revenue());
        for (int i=0; i<buyers.length; ++i)
        {
            assertTrue("For test number " + testNumber + " the server items capacity " + buyers[i].mostItemsAvailable() + " exceeds the limit of 100!", buyers[i].mostItemsAvailable() <= 100);
        }
    }
}
